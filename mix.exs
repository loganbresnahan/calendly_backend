defmodule RssClone.MixProject do
  use Mix.Project

  def project do
    [
      app: :rss_clone,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {RssClone, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 2.6"},
      {:plug, "~> 1.8"},
      {:plug_cowboy, "~> 2.1"},
      {:cors_plug, "~> 2.0"}
    ]
  end
end
