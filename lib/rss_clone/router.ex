defmodule RssClone.Router do
  use Plug.Router
  import Plug.Conn
  require EEx

  plug CORSPlug, origin: ["http://localhost:9000"]
  plug :match
  plug :dispatch

  get "/submit_lead" do
    # Potentially Send back all params after lead creation
    lead_id = "12345"

    send_resp(conn, 200, lead_id)
  end

  get "/schedule_meeting_set" do
    conn = fetch_query_params(conn)

    file = EEx.eval_file(
      "lib/templates/calendly.html.eex",
      assigns: [
        params: conn.query_params
      ]
    )

    conn
    |> put_resp_header("content-type", "text/html; charset=utf-8")
    |> send_resp(200, file)
  end

  match _ do
    send_resp(conn, 404, "404")
  end
end
